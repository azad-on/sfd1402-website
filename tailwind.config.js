module.exports = {
  mode: "jit",
  content: ["./public/**/*.html"],
  theme: {
    extend: {
      fontFamily: {
        vazir: ["Vazirmatn", "Roboto", "Helvetica Neue", "Arial"],
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
